FROM openjdk:18-ea-jdk-slim
EXPOSE 8080
ADD /target/todo-restful-springboot.jar todo-restful-springboot.jar
ENTRYPOINT ["java","-jar","/todo-restful-springboot.jar"]