package pl.paulouski.todospringboot.todo.item.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.paulouski.todospringboot.todo.item.exceptions.InvalidTodoItemParametersException;
import pl.paulouski.todospringboot.todo.item.exceptions.ItemNotFoundException;
import pl.paulouski.todospringboot.todo.item.models.TodoItem;
import pl.paulouski.todospringboot.todo.item.dao.JdbcTodoItemDAO;
import pl.paulouski.todospringboot.todo.list.models.DeleteResponse;

import java.util.List;

@Service
public class TodoItemService {

    private TodoItemValidationService validationService;
    private JdbcTodoItemDAO repository;
    public TodoItemService() {
    }

    @Autowired
    public TodoItemService(TodoItemValidationService validationService, JdbcTodoItemDAO repository) {
        this.validationService = validationService;
        this.repository = repository;
    }

    public TodoItemValidationService getValidationService() {
        return validationService;
    }

    public void setValidationService(TodoItemValidationService validationService) {
        this.validationService = validationService;
    }

    public TodoItem create(String title, String description) {
        var todo = TodoItem.of(title, description);
        return validateItem(todo);
    }

    private TodoItem validateItem(TodoItem todo) {
        if (validationService.isValid(todo)) {
            return todo;
        } else {
            throw new InvalidTodoItemParametersException();
        }
    }

    public void delete(TodoItem item) {
        repository.delete(item);
    }
    public void save(TodoItem item) {
        repository.save(validateItem(item));
    }
    public List<TodoItem> getItemsForList(String listId) {
        return repository.getItemsForList(listId);
    }

    public TodoItem getItem(String listId, String itemId) {
        return repository.getItemsForList(listId)
                .stream()
                .filter(item -> item.getId().equals(itemId))
                .findFirst().orElseThrow(ItemNotFoundException::new);
    }

    public DeleteResponse deleteItemFromList(String id, String itemId) {
        var item = getItem(id, itemId);
        delete(item);
        var msg = String.format("Item '%s' with id '%s' have been deleted", item.getTitle(), itemId);
        return new DeleteResponse(msg);
    }

    public TodoItem updateItemInList(String listId, String itemId, TodoItem updatedItem) {
        update(validateItem(updatedItem), itemId);
        return getItem(listId, itemId);
    }

    private void update(TodoItem todoItem, String itemId) {
        repository.update(todoItem, itemId);
    }
}
