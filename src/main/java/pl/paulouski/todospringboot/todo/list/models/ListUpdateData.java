package pl.paulouski.todospringboot.todo.list.models;

public class ListUpdateData {

    private String title;

    public ListUpdateData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
