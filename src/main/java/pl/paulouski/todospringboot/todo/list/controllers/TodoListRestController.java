package pl.paulouski.todospringboot.todo.list.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import pl.paulouski.todospringboot.todo.item.models.TodoItem;
import pl.paulouski.todospringboot.todo.item.services.TodoItemService;
import pl.paulouski.todospringboot.todo.list.models.DeleteResponse;
import pl.paulouski.todospringboot.todo.list.models.ListUpdateData;
import pl.paulouski.todospringboot.todo.list.models.TodoList;
import pl.paulouski.todospringboot.todo.list.services.TodoListService;

@RestController
@Transactional
public class TodoListRestController {

    private final TodoListService listService;

    private final TodoItemService itemService;
    @Autowired
    public TodoListRestController(TodoListService listService, TodoItemService itemService) {
        this.listService = listService;
        this.itemService = itemService;
    }
    @GetMapping("/list/{id}")
    public ResponseEntity<TodoList> getListById(@PathVariable String id){
        return ResponseEntity.ok(listService.getList(id));
    }
    @DeleteMapping("/list/{id}")
    public ResponseEntity<DeleteResponse> deleteListById(@PathVariable String id){
        return ResponseEntity.ok(listService.delete(id));
    }
    @GetMapping("/list")
    public ResponseEntity<Iterable<TodoList>> getAllLists(){
        return ResponseEntity.ok(listService.getAllLists());
    }
    @PostMapping("/list")
    public ResponseEntity<String> createList(@RequestBody()TodoList list){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(listService.save(list));
    }
    @PutMapping("/list/{id}")
    public ResponseEntity<TodoList> updateList(@PathVariable String id,@RequestBody() ListUpdateData list) {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(listService.updateList(id, list));
    }
    @GetMapping("/list/{id}/item")
    public ResponseEntity<Iterable<TodoItem>> getListItems(@PathVariable String id){
        return ResponseEntity.ok(listService.getListItems(id));
    }
    @GetMapping("/list/{id}/item/{itemId}")
    public ResponseEntity<TodoItem> getItemFromList(@PathVariable String id, @PathVariable String itemId){
        return ResponseEntity.ok(itemService.getItem(id, itemId));
    }

    @PostMapping("/list/{id}/item")
    public ResponseEntity<TodoItem> addItemToList(@PathVariable String id, @RequestBody()TodoItem item){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(listService.addItemToList(id, item));
    }
    @DeleteMapping("/list/{id}/item/{itemId}")
    public ResponseEntity<DeleteResponse> deleteItemFromList(@PathVariable String id, @PathVariable String itemId){
        return ResponseEntity.ok(itemService.deleteItemFromList(id, itemId));
    }

    @PutMapping("/list/{id}/item/{itemId}")
    public ResponseEntity<TodoItem> updateItemFromList(@PathVariable String id, @PathVariable String itemId, @RequestBody()TodoItem item){
        return ResponseEntity.ok(itemService.updateItemInList(id, itemId, item));
    }




}
