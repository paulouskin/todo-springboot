package pl.paulouski.todospringboot.todo.list.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.paulouski.todospringboot.todo.item.models.TodoItem;
import pl.paulouski.todospringboot.todo.item.services.TodoItemService;
import pl.paulouski.todospringboot.todo.list.dao.JdbcTodoListDAO;
import pl.paulouski.todospringboot.todo.list.exceptions.TodoListNotFoundException;
import pl.paulouski.todospringboot.todo.list.models.DeleteResponse;
import pl.paulouski.todospringboot.todo.list.models.ListUpdateData;
import pl.paulouski.todospringboot.todo.list.models.TodoList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TodoListService {
    @Autowired
    JdbcTodoListDAO listDAO;
    @Autowired
    TodoItemService itemService;

    public TodoListService(JdbcTodoListDAO listDAO) {
        this.listDAO = listDAO;
    }

    public JdbcTodoListDAO getListDAO() {
        return listDAO;
    }

    public void setListDAO(JdbcTodoListDAO listDAO) {
        this.listDAO = listDAO;
    }

    public TodoList createList(String title) {
        return new TodoList(title);
    }

    public TodoList getList(String listId) {
        var list =  listDAO.findById(listId).orElseThrow(TodoListNotFoundException::new);
        list.addAll(itemService.getItemsForList(listId));
        return list;
    }

    @Transactional
    public String save(TodoList list) {
        String listId = listDAO.save(list);
        list.getItems().forEach(itemService::save);
        return listId;
    }

    public Iterable<TodoList> getAllLists() {
        return listDAO.findAll();
    }

    @Transactional
    public DeleteResponse delete(String listId) {
        itemService.getItemsForList(listId).forEach(itemService::delete);
        listDAO.delete(listId);
        return new DeleteResponse(String.format("List with Id '%s' have been deleted", listId));
    }

    public List<TodoItem> getListItems(String id) {
        return itemService.getItemsForList(id);
    }

    @Transactional
    public TodoItem addItemToList(String id, TodoItem item) {
        if(listDAO.existById(id)){
            item.setListId(id);
            itemService.save(item);
            return item;
        } else {
            throw new TodoListNotFoundException();
        }

    }

    public TodoList updateList(String id, ListUpdateData listData) {
        Map<String, String> data = new HashMap<>();
        data.put("title", listData.getTitle());
        if (listDAO.existById(id)) {
            listDAO.update(id, data);
            return getList(id);
        }
        else throw new TodoListNotFoundException();
    }
}
