package pl.paulouski.todospringboot.todo.list.controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import pl.paulouski.todospringboot.todo.errorhandlers.ErrorDetails;
import pl.paulouski.todospringboot.todo.item.services.TodoItemService;
import pl.paulouski.todospringboot.todo.list.models.DeleteResponse;
import pl.paulouski.todospringboot.todo.list.models.ListUpdateData;
import pl.paulouski.todospringboot.todo.list.models.TodoList;
import pl.paulouski.todospringboot.todo.list.services.TodoListService;

import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ListRestControllerTest {

    private static final String LIST_ENDPT = "/list";
    @Autowired
    private TodoListService listService;
    @Autowired
    private TodoItemService itemService;
    @Autowired
    private TestRestTemplate template;
    private String listId;
    private TodoList list;


    @BeforeEach
    public void setUp() {
        list = listService.createList("Test list");
        list.add(itemService.create("Test 1", "Description 1"));
        list.add(itemService.create("Test 2", "Description 2"));
        listId = list.getId();
    }

    @Test
    public void shouldGetListById(){
        listService.save(list);
        String query = LIST_ENDPT+ "/" + listId;
        ResponseEntity<TodoList> entity = template.getForEntity(query, TodoList.class);
        Optional<TodoList> list = Optional.ofNullable(entity.getBody());
        list.ifPresent(
                list1 -> assertEquals("Test list", list1.getTitle())
        );
    }
    @Test
    public void shouldDeleteListById() {
        listService.save(list);
        ResponseEntity<DeleteResponse> entity =
                template.exchange(LIST_ENDPT + "/" + listId, HttpMethod.DELETE, null, DeleteResponse.class);
        Optional<DeleteResponse> msg = Optional.ofNullable(entity.getBody());
        msg.ifPresent(Assertions::assertNotNull);

    }
    @Test
    public void shouldGetOnlyListParametersWithoutQuery() {
        listService.save(list);
        ResponseEntity<TodoList[]> response = template.getForEntity(LIST_ENDPT, TodoList[].class);
        Optional<TodoList[]> body = Optional.ofNullable(response.getBody());
        body.ifPresent(
                lists -> Assertions.assertNotNull(lists[0].getId())
        );
    }
    @Test
    public void shouldCreateListAndReturnItsId() {
        ResponseEntity<String> entity = template.postForEntity(LIST_ENDPT, list, String.class);
        Optional<String> actualListId = Optional.ofNullable(entity.getBody());
        actualListId.ifPresent(
                id -> assertEquals(listId, id)
        );
    }
    @Test
    public void shouldFailWhileFetchingNonExistingList() {
        String fakeId = "0000";
        ResponseEntity<ErrorDetails> entity = template.getForEntity(LIST_ENDPT + "/" + fakeId, ErrorDetails.class);
        Optional<ErrorDetails> error = Optional.ofNullable(entity.getBody());
        error.ifPresent(
                errorDetails -> assertEquals("List with specified id not found.", errorDetails.getMessage())
        );
    }
    @Test
    public void shouldUpdateListUsingId() {
        listService.save(list);
        String newTitle = "Updated title for list";
        var updateData = new ListUpdateData();
        updateData.setTitle(newTitle);
        HttpEntity<ListUpdateData> request = new HttpEntity<>(updateData);
        ResponseEntity<TodoList> response = template.exchange(LIST_ENDPT + "/" + listId, HttpMethod.PUT,
                request, TodoList.class);
        Optional<TodoList> body = Optional.ofNullable(response.getBody());
        body.ifPresent(
                list1 -> assertEquals(listId, list1.getId())
        );
    }
}
