package pl.paulouski.todospringboot.todo.list.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import pl.paulouski.todospringboot.todo.list.models.TodoList;
import pl.paulouski.todospringboot.todo.list.services.TodoListService;

import java.util.Optional;

@SpringBootTest
@Transactional
public class JdbcTodoListDAOTest {

    @Autowired
    JdbcTodoListDAO dao;

    @Autowired
    TodoListService service;

    private TodoList list;
    @BeforeEach
    public void testSetUp() {
        list = service.createList("Test list");
        dao.save(list);
    }
    @Test
    public void shouldGetListById() {
        Optional<TodoList> listFromDb = dao.findById(list.getId());
        listFromDb.ifPresent(
                dbList -> Assertions.assertEquals(list.getTitle(), dbList.getTitle())
        );
    }

    @Test
    public void shouldVerifyListExistence() {
        Assertions.assertTrue(dao.existById(list.getId()));
    }

    @Test
    public void shouldDeleteListById() {
        dao.delete(list.getId());
        Assertions.assertFalse(dao.existById(list.getId()));
    }


}
