package pl.paulouski.todospringboot.todo.list.controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import pl.paulouski.todospringboot.todo.errorhandlers.ErrorDetails;
import pl.paulouski.todospringboot.todo.item.models.TodoItem;
import pl.paulouski.todospringboot.todo.item.services.TodoItemService;
import pl.paulouski.todospringboot.todo.list.models.DeleteResponse;
import pl.paulouski.todospringboot.todo.list.models.TodoList;
import pl.paulouski.todospringboot.todo.list.services.TodoListService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ListRestControllerItemTest {

    private static final String ITEM_ENDPT = "/list/%s/item";
    private static final String LIST_ITEM_ENDPT = "/list/%s/item/%s";
    @Autowired
    private TodoListService listService;
    @Autowired
    private TodoItemService itemService;
    @Autowired
    private TestRestTemplate template;
    private String listId;
    private TodoList list;


    @BeforeEach
    public void setUp() {
        list = listService.createList("Test list");
        list.add(itemService.create("Test 1", "Description 1"));
        list.add(itemService.create("Test 2", "Description 2"));
        listId = list.getId();
        listService.save(list);
    }

    @Test
    public void shouldGetAllListItems() {
        String endpoint = String.format(ITEM_ENDPT, listId);
        ResponseEntity<TodoItem[]> response = template.getForEntity(endpoint, TodoItem[].class);
        Optional.ofNullable(response.getBody()).ifPresent(
                items -> assertEquals("Test 1", items[0].getTitle())
        );
    }
    @Test
    public void shouldGetListItemById() {
        var item = list.getItems().get(0);
        String endpoint = String.format(LIST_ITEM_ENDPT, listId, item.getId());
        ResponseEntity<TodoItem> response = template.getForEntity(endpoint, TodoItem.class);
        Optional.ofNullable(response.getBody()).ifPresent(
                item1 -> assertEquals("Test 1", item.getTitle())
        );
    }
    @Test
    public void shouldFailWhileFetchingNonExistingListItem() {
        String fakeId = "62862hjhj288";
        String endpoint = String.format("/list/%s/item/%s", listId, fakeId);
        ResponseEntity<ErrorDetails> response = template.getForEntity(endpoint, ErrorDetails.class);
        Optional.ofNullable(response.getBody()).ifPresent(
                error -> Assertions.assertEquals("Item with specified id not found.", error.getMessage())
        );
    }
    @Test
    public void shouldAddItemIntoList() {
        String endpoint = String.format(ITEM_ENDPT, listId);
        var newItem = itemService.create("Brand new item", "Some interesting need to be done");
        ResponseEntity<TodoItem> response = template.postForEntity(endpoint, newItem, TodoItem.class);
        Optional.ofNullable(response.getBody()).ifPresent(
                item -> Assertions.assertEquals(listId, item.getListId())
        );
    }
    @Test
    public void shouldFailWhenAddInvalidItemIntoList() {
        String endpoint = String.format(ITEM_ENDPT, listId);
        var newItem = new TodoItem("", "Some interesting need to be done");
        ResponseEntity<ErrorDetails> response = template.postForEntity(endpoint, newItem, ErrorDetails.class);
        Optional.ofNullable(response.getBody()).ifPresent(
                body -> Assertions.assertNotNull(body.getMessage())
        );
    }
    @Test
    public void shouldFailWhileAddingItemIntoNonExistingList(){
        String fakeId = "62862hjhj288";
        String endpoint = String.format(ITEM_ENDPT, fakeId);
        var newItem = itemService.create("Brand new item", "Some interesting need to be done");
        ResponseEntity<ErrorDetails> response = template.postForEntity(endpoint, newItem, ErrorDetails.class);
        Optional.ofNullable(response.getBody()).ifPresent(
                body -> Assertions.assertNotNull(body.getMessage())
        );
    }
    @Test
    public void shouldRemoveItemFromTheList() {
        var item = list.getItems().get(0);
        String endpoint = String.format(LIST_ITEM_ENDPT, listId, item.getId());
        ResponseEntity<DeleteResponse> response = template.exchange(
                endpoint, HttpMethod.DELETE, null, DeleteResponse.class
        );
        Optional.ofNullable(response.getBody()).ifPresent(
                body -> Assertions.assertTrue(body.getMessage().contains(item.getId()))
        );
    }
    @Test
    public void shouldUpdateItemInAList() {
        var item = list.getItems().get(0);
        item.setTitle("Updated title");
        HttpEntity<TodoItem> request = new HttpEntity<>(item);
        String endpoint = String.format(LIST_ITEM_ENDPT, listId, item.getId());
        ResponseEntity<TodoItem> response = template.exchange(
                endpoint, HttpMethod.PUT, request, TodoItem.class
        );
        Optional.ofNullable(response.getBody()).ifPresent(
                body -> Assertions.assertEquals(item.getId(), body.getId())
        );
    }

    @Test
    public void shouldNotUpdateItemWithInvalidTitle() {
        var item = list.getItems().get(0);
        item.setTitle("");
        HttpEntity<TodoItem> request = new HttpEntity<>(item);
        String endpoint = String.format(LIST_ITEM_ENDPT, listId, item.getId());
        ResponseEntity<ErrorDetails> response = template.exchange(
                endpoint, HttpMethod.PUT, request, ErrorDetails.class
        );
        Optional.ofNullable(response.getBody()).ifPresent(
                body -> Assertions.assertNotNull(body.getMessage())
        );
    }
}
